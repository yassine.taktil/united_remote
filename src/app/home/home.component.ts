import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Repository } from '../models/repository';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  today: Date;
  last_month_day: string;
  repositories: Array<Repository> = [];
  repository: Repository;

  constructor(private apiService: ApiService) {
    this.today = new Date();
    
  }

  ngOnInit() {
    this.today.setMonth(this.today.getMonth() - 1);

    //Get Date Format as requested in the Github URL
    this.last_month_day = this.today.toISOString().slice(0, 10);

    this.getRepos(this.last_month_day, 1);
  }

  getRepos(date, page) {
    this.apiService.getRepos(date, page).subscribe((data) => {

      //Fetch repositories and stock only wanted attributes to our repository class
      data["items"].forEach(item => {
        this.repository = new Repository();
        var item_created = new Date(item.created_at)
        var today = new Date()

        //Get days difference between today and the day the repository is created
        var timeDiff = Math.abs(today.getTime() - item_created.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)).toString();

        //Test stars count if it's thousands then "k" as a representer for thousands
        item.stargazers_count > 999 ? this.repository.stars = ( item.stargazers_count/1000).toFixed(1) + 'k' : this.repository.stars = item.stargazers_count;

        //Same thing for issues
        item.open_issues_count > 999 ? this.repository.issues = ( item.open_issues_count/1000).toFixed(1) + 'k' : this.repository.issues = item.open_issues_count;

        this.repository.name = item.name;
        this.repository.description = item.description;
        this.repository.created = diffDays;
        this.repository.avatar = item.owner.avatar_url;

        this.repositories.push(this.repository);

      });
      console.log(this.repositories);
    })
  }

}
