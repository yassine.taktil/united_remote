export class Repository {
    name: string;
    description: string;
    stars: string;
    issues: string;
    avatar: string;
    created: string;
}