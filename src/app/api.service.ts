import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { API_CONFIG } from './api-config';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient ) { }

  getRepos(date,page){
    return this.http.get(API_CONFIG.url + date + "&sort=stars&order=desc&page=" + page);
  }
}
